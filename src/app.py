import os
from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from .extensions import db, jwt, ma
from .resources import resources
from .config import Config, TestConfig


def create_app(config=Config):
    app = Flask(__name__)
    app.config.from_object(config)
    api = Api(app, prefix=os.getenv("API_ENDPOINT"))
    config_resource(api)
    config_extensions(app)
    return app


def config_resource(api):
    for res in resources:
        try:
            api.add_resource(res, *res.URL)
            print('\n### Resource: {}, Endpoints: {}\n'.format(res.__name__, res.URL))
        except RuntimeError as err:
            print('\n### ERROR!! {} => Resource: {}, Endpoints: {}\n'.format(err, res.__name__, res.URL))


def config_extensions(app):
    db.init_app(app)
    jwt.init_app(app)
    ma.init_app(app)
    CORS(app)
