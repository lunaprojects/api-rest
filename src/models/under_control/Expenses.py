from sqlalchemy import Column, Integer, TIMESTAMP, String, ForeignKey, Numeric
from src.models import Model, User
from src.common.Utils import get_current_datetime
from src.extensions import db


class Expenses(db.Model, Model):

    __tablename__ = 'app_uc_expenses'

    id = Column(Integer, primary_key=True)
    category = Column(String(150), default="No category")
    name = Column(String(100))
    amount = Column(Numeric(50, 10), default=0)
    date_at = Column(TIMESTAMP, nullable=True)
    created_by = Column(Integer, ForeignKey(User.id))
    created_at = Column(TIMESTAMP, default=get_current_datetime())
    updated_at = Column(TIMESTAMP, default=get_current_datetime())
