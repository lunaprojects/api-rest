from datetime import datetime
from src.extensions import db


class Model(object):
    __idname__ = 'id'

    def __init__(self, data={}):
        self.load_from_dict(data)
        self.created_at = datetime.now()

    def load_from_dict(self, data):
        for key, value in data.items():
            self.setattr(key.lower(), value)

    def setattr(self, key, value):
        try:
            setattr(self, key, value)
        except:
            pass

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        return True

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def create_if_not_exists(cls):
        db.engine.execute('SET FOREIGN_KEY_CHECKS = 0')
        if db.engine.dialect.has_table(db.engine, cls.__tablename__):
            db.metadata.drop_all(db.engine, tables=[cls.__table__])
        db.metadata.create_all(db.engine, tables=[cls.__table__])

