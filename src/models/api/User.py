from sqlalchemy import Column, Integer, TIMESTAMP, String, ForeignKey, BOOLEAN
from werkzeug.security import generate_password_hash, check_password_hash
from src.common.Utils import get_current_datetime
from src.extensions import db
from .. import Model
from .Role import Role


class User(Model, db.Model):
    __tablename__ = 'app_users'

    id = Column(Integer, primary_key=True)
    status = Column(BOOLEAN, default=True)
    name = Column(String(60))
    _username = Column('username', String(20), unique=True, nullable=False)
    _password = Column('password', String(255), nullable=False)
    email = Column(String(150), unique=True)
    role_id = Column(Integer, ForeignKey(Role.id), default=1)
    created_at = Column(TIMESTAMP, default=get_current_datetime())
    updated_at = Column(TIMESTAMP, default=get_current_datetime())

    def _get_username(self):
        return self._username

    def _set_username(self, username):
        self._username = username.lower()

    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = generate_password_hash(password)

    username = db.synonym('_username', descriptor=property(_get_username, _set_username))
    password = db.synonym('_password', descriptor=property(_get_password, _set_password))

    def check_password(self, password):
        if self.password is None:
            return False
        return check_password_hash(self.password, password)

    def get_role(self):
        return Role.query.get(self.role_id)

    @property
    def name_attr(self):
        return self.name

    @name_attr.setter
    def name_attr(self, _name):
        self.name = _name

    def json(self, token):
        return {
            'id': self.id,
            'email': self.email,
            'fullname': self.name,
            'username': self.username,
            'status': self.status,
            'roles': self.get_role().name,
            'access_token': token
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_username(cls, _username):
        return cls.query.filter_by(username=_username).first()

    @classmethod
    def find_by_email(cls, _email):
        return cls.query.filter_by(email=_email).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()
