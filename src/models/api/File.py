from sqlalchemy import Column, Integer, TIMESTAMP, String

from src.common.Utils import get_current_datetime
from src.extensions import db
from src.models import Model


class File(db.Model, Model):
    __tablename__ = 'app_files'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=True)
    url = Column(String(60), nullable=True)
    extension = Column(String(30))
    created_at = Column(TIMESTAMP, default=get_current_datetime())
    updated_at = Column(TIMESTAMP, default=get_current_datetime())
