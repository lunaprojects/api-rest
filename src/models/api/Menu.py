from sqlalchemy import Column, Integer, TIMESTAMP, String, JSON

from src.common.Utils import get_current_datetime
from src.extensions import db
from src.models import Model


class Menu(Model, db.Model):
    __tablename__ = 'app_menus'

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, default=1)
    name = Column(String(60), nullable=True)
    component = Column(String(60), nullable=True)
    icon_type = Column(String(30), default='question')
    breadcrumb = Column(JSON)
    description = Column(String(150))
    order = Column(Integer, default=1)
    created_at = Column(TIMESTAMP, default=get_current_datetime())
    updated_at = Column(TIMESTAMP, default=get_current_datetime())

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()
