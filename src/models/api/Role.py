from sqlalchemy import Column, Integer, TIMESTAMP, String, BOOLEAN
from src.common.Utils import get_current_datetime
from src.models import Model
from src.extensions import db


class Role(Model, db.Model):
    __tablename__ = 'app_roles'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), default=None)
    description = Column(String(150), default=None)
    created_at = Column(TIMESTAMP, default=get_current_datetime())
    updated_at = Column(TIMESTAMP, default=None, onupdate=get_current_datetime())
