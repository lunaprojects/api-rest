import os
from datetime import timedelta
from dotenv import load_dotenv


def load_env():
    env_path = os.getcwd() + '/.env'
    load_dotenv(dotenv_path=env_path)


class Config(object):
    load_env()
    DEBUG = os.getenv("APP_DEBUG") if os.getenv("APP_DEBUG") else False
    ENV = str(os.getenv("APP_ENV"))
    UPLOAD_FOLDER = os.getenv("UPLOAD_FOLDER")
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'xls', 'xlsx'}

    # DB SETTINGS
    DB_FLUSH_LIMIT = 10000
    DB_USERNAME = os.getenv("DB_USERNAME") if os.getenv("DB_USERNAME") else 'db_user'
    DB_PASSWORD = os.getenv("DB_PASSWORD") if os.getenv("DB_PASSWORD") else 'db_password'
    DB_HOST = os.getenv("DB_HOST") if os.getenv("DB_HOST") else 'localhost'
    DB_PORT = int(os.getenv("DB_PORT")) if os.getenv("DB_PORT") else 3306
    DB_DATABASE = os.getenv("DB_DATABASE") if os.getenv("DB_PORT") else 'db_name'

    # SQLALCHEMY CONFIGURATIONS
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8'.format(DB_USERNAME, DB_PASSWORD, DB_HOST,
                                                                                DB_PORT, DB_DATABASE)
    SQLALCHEMY_BINDS = ''
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # JWT TOKENS CONFIGURATION
    JWT_SECRET_KEY = os.getenv("SECRET_KEY")
    JWT_TOKEN_LOCATION = 'headers'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(
        seconds=int(os.getenv("TOKEN_EXPIRES")) if os.getenv("TOKEN_EXPIRES") else 6000)


class TestConfig(object):
    load_env()

    DEBUG = os.getenv("APP_DEBUG") if os.getenv("APP_DEBUG") else False
    ENV = os.getenv("APP_ENV")
    UPLOAD_FOLDER = os.getenv("UPLOAD_TEST_FOLDER")
    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'xls', 'xlsx'}

    # DB SETTINGS
    DB_FLUSH_LIMIT = 10000
    DB_USERNAME = os.getenv("DB_TEST_USERNAME") if os.getenv("DB_TEST_USERNAME") else 'db_user'
    DB_PASSWORD = os.getenv("DB_TEST_PASSWORD") if os.getenv("DB_TEST_PASSWORD") else 'db_password'
    DB_HOST = os.getenv("DB_TEST_HOST") if os.getenv("DB_TEST_HOST") else 'localhost'
    DB_PORT = int(os.getenv("DB_TEST_PORT")) if os.getenv("DB_TEST_PORT") else 3306
    DB_DATABASE = os.getenv("DB_TEST_DATABASE") if os.getenv("DB_TEST_PORT") else 'db_name'

    # SQLALCHEMY CONFIGURATIONS
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}:{}/{}?charset=utf8'.format(DB_USERNAME, DB_PASSWORD, DB_HOST,
                                                                                   DB_PORT, DB_DATABASE)
    SQLALCHEMY_BINDS = ''
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # JWT TOKENS CONFIGURATION
    JWT_SECRET_KEY = os.getenv("SECRET_TEST_KEY")
    JWT_TOKEN_LOCATION = 'headers'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(
        seconds=int(os.getenv("TOKEN_TEST_EXPIRES")) if os.getenv("TOKEN_TEST_EXPIRES") else 6000)

