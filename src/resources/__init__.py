from .api import UserResource, Auth, MenuResource, UploadFile
from .http import Expenses

resources = [Auth, UserResource, MenuResource, Expenses, UploadFile]

