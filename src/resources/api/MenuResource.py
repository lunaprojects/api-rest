from flask_jwt_extended import jwt_required
from flask_restful import Resource

from ..schemas import MenuSchema
from ...models import Menu


class MenuResource(Resource):
    URL = ['/menu', '/menu/<int:menu_id>']

    def __init__(self):
        self.schema = MenuSchema()

    @jwt_required
    def get(self, menu_id=None):

        menus = self.schema.dump(Menu.find_all(), many=True).data

        if 'key' not in menus:
            for element in menus:
                if 'key' not in element:
                    element['key'] = str(element['id'] * 13)

        return {'data': menus}
