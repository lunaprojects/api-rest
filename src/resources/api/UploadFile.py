from flask_restful import Resource
from flask import request
from flask_jwt_extended import jwt_required
from werkzeug.utils import secure_filename
import os

from src.common import ProcessFile, allowed_file
from ...extensions import db
from ...models import File as Model, Expenses


class UploadFile(Resource):
    URL = ['/upload', '/upload/<string:file>']

    def __init__(self):
        self.model = Model()

    # @jwt_required
    def get(self, file):
        pass

    # @jwt_required
    def post(self, extension=None):
        if 'file' not in request.files:
            return "No file!"
        file = request.files['file']
        if file.filename == '':
            return "No filename!"

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            file.save(os.getenv("UPLOAD_FOLDER") + "/" + filename)

            self.model.name = filename
            self.model.url = os.getenv("UPLOAD_FOLDER")
            db.session.add(self.model)
            db.session.commit()

            file = ProcessFile(filename, os.getenv("UPLOAD_FOLDER"), 0, 4)
            files = file.process()

            print(files)

            for row in files:
                r = Expenses()
                r.name = row.get('name')
                r.amount = row.get('amount')
                r.date_at = row.get('date_at')
                db.session.add(r)
                db.session.commit()

            return "File saved"
