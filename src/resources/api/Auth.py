from flask import request, abort
from flask_jwt_extended import jwt_required, create_access_token
from flask_restful import Resource, reqparse

from src.models.api.User import User


class Auth(Resource):
    URL = ['/auth']

    parser = reqparse.RequestParser()
    msg = 'This field cannot be blank.'

    parser.add_argument('username',
                        type=str,
                        required=True,
                        help=msg)

    parser.add_argument('password',
                        type=str,
                        required=True,
                        help=msg)

    # @jwt_required
    def get(self):
        pass

    def post(self):
        if not request.is_json:
            return abort(500, "Missing JSON in request")

        data = self.parser.parse_args()

        user = User.find_by_username(data['username'])

        if not user:
            return {'message': 'Bad username or password.'}, 401
        if not user.check_password(data['password']):
            return {'message': 'Bad username or password.'}, 401

        token = create_access_token(identity=user.username)

        user = user.json(token)

        if 'access_token' not in user:
            user['access_token'] = token
        return {'data': user}

    def put(self):
        pass

    def delete(self):
        pass
