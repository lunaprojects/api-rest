from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
import datetime
from ..schemas import UserSchema
from ...models import User

FORMAT = '%d-%m-%Y'


class UserResource(Resource):
    URL = ['/user', '/user/<int:user_id>']

    parser = reqparse.RequestParser()

    parser.add_argument('username',
                        type=str,
                        required=True,
                        help='This field cannot be blank.')

    parser.add_argument('password',
                        type=str,
                        required=True,
                        help='This field cannot be blank.')

    parser.add_argument('role_id',
                        default=1,
                        type=int,
                        required=True,
                        help='This field is required.')

    def __init__(self):
        self.model = User()
        self.schema = UserSchema()

    @jwt_required
    def get(self, user_id=None):
        """
            :param user_id: not required parameter, filter the return data.
            :return: Data from users filtering by primary key or not
        """
        if user_id is not None:
            return {'data': self.schema.dump(User.find_by_id(user_id)).data}

        users = self.schema.dump(User.find_all(), many=True).data

        for user in users:
            if 'key' not in user:
                user['key'] = str(user['id'] * 5)
            date = datetime.datetime.strptime(user['created_at'][:19], '%Y-%m-%dT%H:%M:%S')
            user['created_at'] = date.strftime('%d/%m/%Y')

        return {'data': users}

    @jwt_required
    def post(self):
        data = self.parser.parse_args()
        if User.find_by_username(data['username']):
            return {'message': 'A user with that {} already exists'.format(data['username'])}, 400

        if User(data).save_to_db():
            return {'action': 'create', 'message': 'User created successfully.',
                    'data': self.schema.dump(User.find_by_username(data['username']), many=True).data}, 201

        return {'message': 'Bad request.'}, 400

    @jwt_required
    def put(self):
        pass
