from flask_jwt_extended import jwt_required
from flask_restful import Resource
from ...schemas import ExpensesSchema
from ....models import Expenses as Model
from ....common import *


class Expenses(Resource):
    URL = ['/expenses']

    QUERY_CHART = """
        SELECT category label, FORMAT(SUM(amount) / 1000 , 2) value 
        FROM api_rest.uc_expenses 
        GROUP BY category
        ORDER BY SUM(amount) DESC
    """

    def __init__(self):
        self.model = Model()
        self.schema = ExpensesSchema()

    # @jwt_required
    def get(self):
        expenses = self.schema.dump(self.model.query.all(), many=True).data

        chart_details = {
            'caption': 'Ingresos & Gastos',
            'subCaption': 'Año 2018',
            'xAxisName': 'Categorias',
            'yAxisName': 'Importe €',
            'numberSuffix': 'K',
            'theme': 'fusion'
        }

        if 'key' not in expenses:
            for element in expenses:
                if 'key' not in element:
                    element['key'] = str(element['id'] * 13)

        with MariaDB() as mdb:
            data_source = mdb.exec_query(self.QUERY_CHART)

        charts = FusionChart(kind='column2d', width='700', height='400', chart_details=chart_details,
                             data=data_source).get_chart()

        return {
            'charts': charts,
            'data': expenses
        }
