from marshmallow_sqlalchemy import ModelSchema, field_for
from marshmallow import fields
from ...models.api import User


class UserSchema(ModelSchema):
    username = field_for(User, '_username', dump_only=True)
    password = field_for(User, '_password', dump_only=True)

    class Meta:
        model = User

        fields = ('id', 'email', 'username', 'name', 'created_at', 'status')
