from marshmallow_sqlalchemy import ModelSchema, field_for
from marshmallow import fields
from src.models.under_control import Expenses


#
# @author David Luna
#
class ExpensesSchema(ModelSchema):
    amount = fields.Float()

    class Meta:
        model = Expenses
        fields = ('id', 'category', 'amount', 'date_at', 'created_by', 'created_at')
