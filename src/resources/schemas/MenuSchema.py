from marshmallow_sqlalchemy import ModelSchema

from ...models.api import Menu


class MenuSchema(ModelSchema):
    class Meta:
        model = Menu
        fields = ('id', 'name', 'parent_id', 'component', 'description', 'icon_type', 'order')
