import mysql.connector as mariadb
import os


class MariaDB(object):
    def __init__(self):
        self.cursor = None

    def __enter__(self):
        try:
            self.conn = mariadb.connect(
                user=os.getenv("DB_USERNAME"),
                password=os.getenv("DB_PASSWORD"),
                host=os.getenv("DB_HOST"))
            self._init_cursor_()
            return self
        except mariadb.Error as err:
            print("Error connecting database. %s", err)

    def _init_cursor_(self):
        self.cursor = self.conn.cursor(dictionary=True)

    def exec_query(self, query):
        try:
            self.cursor.execute(query)
            data = self.cursor.fetchall()
            return data
        except mariadb.Error as err:
            print("Error executing statement: %s, error: %s" % (query, err))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.close()
        self.conn.close()
