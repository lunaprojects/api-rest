import os, json


class FusionChart(object):
    """
        type: 'column2d',// The chart type
        width: '700', // Width of the chart
        height: '400', // Height of the chart
        dataFormat: 'json', // Data type
        dataSource: {
            // Chart Configuration
            "chart": {
                "caption": "Ingresos & Gastos",
                "subCaption": "Año 2018",
                "xAxisName": "Categorias",
                "yAxisName": "Importe €",
                "numberSuffix": "K",
                "theme": "fusion",
            },
            // Chart Data
            "data": expenses.charts
        }
    """

    def __init__(self, kind, width, height, chart_details, data):
        self.kind = kind
        self.width = width if width else '700'
        self.height = height if height else '400'
        self.data_format = 'json'
        self.chart_details = chart_details
        self.data = data

    def get_chart(self):
        return {
            'type': self.kind,
            'width': self.width,
            'height': self.height,
            'dataFormat': self.data_format,
            'dataSource': self._get_data_source()
        }

    def _get_data_source(self):
        return {
            'chart': self.chart_details,
            'data': self.data
        }
