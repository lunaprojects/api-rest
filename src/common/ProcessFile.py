from xlrd import open_workbook
import os
from datetime import datetime
from collections import OrderedDict
import simplejson as json


class ProcessFile(object):
    DEFAULT_INDEX = 0

    def __init__(self, file_name, folder, start_index, line_start):
        self.file_name = file_name
        self.folder = folder
        self.start_index = start_index
        self.line_start = line_start

    def _get_file_path(self):
        return os.path.join(self.folder, self.file_name)

    def get_file(self):
        return self.file_name

    def process(self):
        wb = open_workbook(self._get_file_path())
        sh = wb.sheet_by_index(self.start_index)
        # List to hold dictionaries
        array = []
        # Iterate through each row in worksheet and fetch values into dict
        for row in range(self.line_start, sh.nrows):
            data = {}
            row_values = sh.row_values(row)
            data['date_at'] = datetime.strptime(row_values[1], '%d/%m/%Y').isoformat()
            data['name'] = row_values[2]
            data['amount'] = float(row_values[3])
            array.append(data)
        # Serialize the list of dicts to JSON
        j = json.dumps(array)

        # Write to file
        with open(self.folder + os.sep + self.file_name.split('.')[0] + '.json', 'w') as f:
            f.write(j)

        return array