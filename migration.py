import os
import sys

from src.app import create_app
from src.extensions import db
from src.models import models, User, Role, Menu

if __name__ == '__main__':
    app = create_app()

    if 'createdb' in sys.argv:
        with app.app_context():
            for model in models:
                model.create_if_not_exists()
                print('\n### Database migration successful! ###\n # Model: {}, Table: {}\n'.format(model.__name__,
                                                                                                   model.__tablename__))

    if 'seeddb' in sys.argv:
        with app.app_context():
            # noinspection PyArgumentList
            try:
                role = Role({'id': 1, 'name': 'Administrator', 'description': 'Total control'})
                db.session.add(role)
                db.session.commit()
                user = User({'id': 1, 'name': 'Administrator', 'email': os.getenv("ADMIN_EMAIL"), 'username': 'admin',
                             'password': os.getenv("ADMIN_PWD"), 'role_id': 1})
                db.session.add(user)
                db.session.commit()

                menu = Menu(
                    {'id': 1, 'parent_id': 1, 'name': 'Configuration', 'component': 'Config', 'icon_type': 'settings',
                     'description': 'App configuration', 'order': 1})
                db.session.add(menu)
                db.session.commit()

                print('\n### Database seeded! ###\n')
            except RuntimeError as err:
                print('\n### Insert data error! {}###\n').format(err)
