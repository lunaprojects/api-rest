### Installations

Linux basics:

```
sudo apt install build-essential
sudo apt install python-dev
sudo apt install libmysqlclient-dev
sudo apt install python3-dev
sudo apt install python-wheel
sudo apt install python3-venv
```
Clone instructions >> 
Set environ vars:
```
git clone git@bitbucket.org:lunaprojects/api-rest.git app
cp app/setup/.env.example .env 
```

Environment:
```
python3 -m venv ./venv
source venv/bin/activate
```

Install dependencies:
```
pip install wheel
pip install -r app/setup/requirements.txt
```
Create and seed database:
```
python app/migration.py createdb
python app/migration.py seeddb
```
Run app:
```
python app/run.py
```
