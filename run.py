import os
from src.app import create_app
from src.config import TestConfig, Config

conf = Config

clear = lambda: os.system('clear')


if __name__ == '__main__':

    port = os.getenv("APP_PORT") if os.getenv("APP_PORT") else 5000
    debug = os.getenv("APP_DEBUG") if os.getenv("APP_DEBUG") else True
    clear()
    if debug:
        conf = TestConfig
    app = create_app(TestConfig)
    app.run(host='0.0.0.0', port=port, debug=debug, threaded=True)  # use_reloader=True
